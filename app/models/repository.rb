# frozen_string_literal: true

class Repository
  class << self
    def import_parliamentaries(file)
      items = []
      names = []
      CSV.foreach(file, col_sep: ";", encoding: "bom|utf-8", quote_char: '"', headers: :first_row) do |row|
        attributes = row.to_h.deep_symbolize_keys

        if attributes[:sgUF] == "SP"
          parliamentary = attributes[:txNomeParlamentar]

          items << new_parliamentary(attributes) if names.exclude?(parliamentary)

          names << parliamentary
        end
      end
      Parliamentary.import(items)
    end

    def import_spendings(file)
      items = []
      last_name = ""
      parliamentary = ""
      CSV.foreach(file, col_sep: ";", encoding: "bom|utf-8", quote_char: '"', headers: :first_row) do |row|
        attributes = row.to_h.deep_symbolize_keys

        if attributes[:sgUF] == "SP"
          parliamentary_name = attributes[:txNomeParlamentar]

          if last_name != parliamentary_name
            parliamentary = Parliamentary.where(name: parliamentary_name).first
            last_name = parliamentary_name
          end

          spending = new_spending(attributes)

          spending.parliamentary = parliamentary
          items << spending
        end
      end
      Spending.import(items)
    end

    private
      def new_parliamentary(attributes)
        Parliamentary.new(
          name: attributes[:txNomeParlamentar],
          register: attributes[:ideCadastro],
          party: attributes[:sgPartido]
        )
      end

      def new_spending(attributes)
        Spending.new(
          description: attributes[:txtDescricao],
          provider: attributes[:txtFornecedor],
          date: attributes[:datEmissao],
          value: attributes[:vlrLiquido],
          document_url: attributes[:urlDocumento]
        )
      end
  end
end
