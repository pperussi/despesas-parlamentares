# frozen_string_literal: true

class Parliamentary < ApplicationRecord
  has_many :spendings
  has_one_attached :photo

  validates :name, :register, :party, presence: true

  validates :name, uniqueness: true

  def get_photo
    begin
      downloaded_photo = Down.download(
        "http://www.camara.leg.br/internet/deputado/bandep/#{self.register}.jpg"
      )
    rescue Down::NotFound
      return
    end

    self.photo.attach(
      io: downloaded_photo, filename: "#{self.name}.jpg"
    )
  end

  def spendings_sum
    spendings_sum = 0
    spendings = self.spendings

    spendings.each do |spending|
      spendings_sum += spending.value
    end

    spendings_sum
  end

  def most_expensive_spending
    value_list = []
    spendings = self.spendings

    spendings.each do |spending|
      value_list << spending.value
    end

    value_list.sort! { |x, y| y <=> x }
    value_list[0]
  end
end
