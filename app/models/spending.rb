# frozen_string_literal: true

class Spending < ApplicationRecord
  belongs_to :parliamentary

  validates :description, :provider, :date, :value,
            :document_url, presence: true

  validates :document_url, uniqueness: true
end
