# frozen_string_literal: true

class HomeController < ApplicationController
  def index; end

  def add_data; end

  def import_csv
    begin
     @parliamentaries = Repository.import_parliamentaries(params[:file])

     @spendings = Repository.import_spendings(params[:file])
   rescue StandardError
     flash[:alert] = "N\u00E3o foi poss\u00EDvel adicionar as informa\u00E7\u00F5es"
     redirect_to :add_data
     return
   end

    redirect_to parliamentaries_path
  end
end
