# frozen_string_literal: true

class ParliamentariesController < ApplicationController
  def index
    @parliamentaries = Parliamentary.all
  end

  def show
    @parliamentary = Parliamentary.find(params[:id])
  end
end
