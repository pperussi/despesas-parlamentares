class ChangeDateTypeToDatetime < ActiveRecord::Migration[6.0]
  def change
    change_column :spendings, :date, :datetime
  end
end
