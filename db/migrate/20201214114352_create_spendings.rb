# frozen_string_literal: true

class CreateSpendings < ActiveRecord::Migration[6.0]
  def change
    create_table :spendings do |t|
      t.string :description
      t.string :provider
      t.date :date
      t.float :value
      t.string :document_url
      t.references :parliamentary, null: false, foreign_key: true

      t.timestamps
    end
  end
end
