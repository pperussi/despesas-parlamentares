# frozen_string_literal: true

class CreateParliamentaries < ActiveRecord::Migration[6.0]
  def change
    create_table :parliamentaries do |t|
      t.string :name
      t.integer :register
      t.string :party

      t.timestamps
    end
  end
end
