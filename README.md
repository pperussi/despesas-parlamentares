# Desafio Backend
  O projeto Despesas-Parlamentares é resultado de Desafio Backend da AgendaEdu, que consiste em importar as informações das despesas da Cota para o Exercício da Atividade Parlamenta(CEAP) dos Deputados Federais, no caso de São Paulo, do ano de 2019 para que possam ser vizualizadas de uma forma mais acessível, além de incentivar a fiscalização dos gastos com dinheiro público, assim como a disponibilização de dados abertos para consulta popular.

## Como executar
  Para iniciar o projeto:

  ```sh
  docker-compose up -d
  ```

  Rodar a migração do banco de dados(necessário antes de acessar a url)

  ```
  docker-compose exec rails rails db:migrate

  ```

  Para acessar a aplicação no navegador:

  ```
  localhost:3000
  ```

  Deletando container:

  ```
  docker-compose down
  ```


## Notas
- A importação do arquivo CSV da câmara dos Deputados leva em média 1 minuto para carregar todas as informações
- O método `get_photo` do model `Parliamentary` foi feita na tentativa de armazenar a foto de cada parlamentar, fazendo o donwload da imagem com a gem `Down` e salvando no banco com `ActiveStorage` para facilitar a visualização do perfil de cada deputado. Porém devido a enorme lentidão que adicionou ao processo de importação das informações teve que ser depreciada.
- A opção escolhida então foi a de carregar a foto dos parlamentares direto da `url` da câmara em cada perfil