# frozen_string_literal: true

Rails.application.routes.draw do
  root to: "home#index"
  get "add_data", to: "home#add_data"
  post "import_csv", to: "home#import_csv"

  resources :parliamentaries, only: %i[index show]
end
