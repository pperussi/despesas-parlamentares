# frozen_string_literal: true

require "rails_helper"

RSpec.describe Parliamentary do
  describe ".get_photo" do
    it "downloads a parliamentary photo and attach it" do
      photo = File.new("spec/person.jpg")

      allow(Down).to receive(:download).and_return(photo)

      parliamentary = Parliamentary.new(
        name: "Maria da Silva",
        register: 12345,
        party: "PARTIDO"
      )

      parliamentary.get_photo
      parliamentary.save

      expect(parliamentary.photo.attached?).to be true
    end

    it "continues the parliamentary creation when the photo does not exists" do
      parliamentary = Parliamentary.create!(
        name: "Joana Santos",
        register: 54321,
        party: "PARTIDO"
      )

      expect(parliamentary.photo.attached?).to be false
      expect(Parliamentary.count).to eq(1)
    end
  end

  describe ".spendings_sum" do
    it "sums all the spendings of a parliamentary" do
      parliamentary = build(:parliamentary_with_spendings)

      expect(parliamentary.spendings_sum).to eq(2551.1)
    end
  end

  describe ".most_expensive_spending" do
    it "returns the most expensive spending of a parliamentary" do
      parliamentary = build(:parliamentary_with_spendings)

      expect(parliamentary.most_expensive_spending).to eq(1500.20)
    end
  end
end
