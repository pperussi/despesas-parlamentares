# frozen_string_literal: true

require "rails_helper"

RSpec.describe Repository do
  describe "#import_parliamentaries" do
    it "imports only informations of S\u00E3o Paulo state from a csv file" do
      document = "spec/test.csv"
      described_class.import_parliamentaries(document)

      expect(Parliamentary.count).to eq(2)
    end

    it "raises an error when try to duplicate some register" do
      document = "spec/test.csv"
      described_class.import_parliamentaries(document)

      expect do
        described_class.import_parliamentaries(document).to_rase(
          ActiveRecord::RecordInvalid, "Validation failed: Name has already been taken"
        )
      end
    end
  end

  describe "#import_spendings" do
    it "imports only spendings of registered parliamentaries" do
      document = "spec/test.csv"
      described_class.import_parliamentaries(document)
      described_class.import_spendings(document)

      expect(Parliamentary.second.spendings.count).to eq(22)
    end

    it "raises an error when try to duplicate some register" do
      document = "spec/test.csv"
      described_class.import_parliamentaries(document)
      described_class.import_spendings(document)

      expect do
        described_class.import_spendings(document).to_rase(
          ActiveRecord::RecordInvalid,
          "Validation failed: Document_url has already been taken"
        )
      end
    end
  end
end
