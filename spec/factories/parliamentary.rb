# frozen_string_literal: true

FactoryBot.define do
  factory :parliamentary, class: Parliamentary do
    id { 1 }
    name { "Jos\u00E9 da Silva" }
    register { 12345  }
    party { "PARTIDO" }

    factory :parliamentary_with_spendings do
      spendings {
        [
         association(:spending),
         association(:spending, :second),
         association(:spending, :third)
        ]
      }
    end
  end
end
