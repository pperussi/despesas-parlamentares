# frozen_string_literal: true

FactoryBot.define do
  factory :spending, class: Spending do
    description { "Compras inform\u00E1tica" }
    provider { "Inform\u00E1ticas TI"  }
    date { 2020 - 03 - 12 }
    value { 1500.20 }
    document_url {  "https://www.camara.leg.br/cota-parlamentar/documentos/publ/1234/2020/1234567.pdf" }
    parliamentary

    trait :second do
      value { 300.90 }
      document_url {  "https://www.camara.leg.br/cota-parlamentar/documentos/publ/1235/2020/1234568.pdf" }
    end

    trait :third do
      value { 750.00 }
      document_url {  "https://www.camara.leg.br/cota-parlamentar/documentos/publ/1236/2020/1234569.pdf" }
    end
  end
end
